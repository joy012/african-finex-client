import { Box, makeStyles } from "@material-ui/core";
import React, { createContext } from "react";
import { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import './App.css';
import BuyFinalPage from "./components/Buy/BuyFinalPage";
import BuyFourthPage from "./components/Buy/BuyFourthPage";
import BuyFrontPage from "./components/Buy/BuyFrontPage";
import BuySecondPage from "./components/Buy/BuySecondPage";
import BuyThirdPage from "./components/Buy/BuyThirdPage";
import BuyPdf from "./components/BuyPdf/BuyPdf";
import SellPdf from "./components/SellPdf";
import Navbar from "./components/Navbar/Navbar";
import SellFinalPage from "./components/Sell/SellFinalPage";
import SellFourthPage from "./components/Sell/SellFourthPage";
import SellFrontPage from "./components/Sell/SellFrontPage";
import SellSecondPage from "./components/Sell/SellSecondPage";
import SellThirdPage from "./components/Sell/SellThirdPage";

export const UserContext = createContext();

const useStyles = makeStyles((theme) => ({
  card: {
    width: '26%',
    display: 'block',
    margin: 'auto',
    padding: '0rem!important',
    [theme.breakpoints.down('md')]: {
      width: '35%'
    },
    [theme.breakpoints.down('sm')]: {
      width: '45%'
    },
    [theme.breakpoints.down('xs')]: {
      width: '80%'
    },
  },
}));

function App() {
  const detectingUserLang = navigator.language || navigator.userLanguage;
  const userLangFirst = detectingUserLang.split("-")[0];
  const classes = useStyles();
  const [country, setCountry] = useState({
    name: '',
    currency: '',
    trueCurrency: ''
  });
  const [wallet, setWallet] = useState('');
  const [quantity, setQuantity] = useState(0);
  const [iban, setIban] = useState('');
  const [TXid, setTXid] = useState('');
  const [bank, setBank] = useState('');
  const [orderId, setOrderId] = useState('');
  const [time, setTime] = useState('');
  const [language, setLanguage] = useState(userLangFirst);
  const [realQuantity, setRealQuantity] = useState(0);


  return (
    <UserContext.Provider value={[country, setCountry, quantity, setQuantity, wallet, setWallet, iban, setIban, TXid, setTXid, bank, setBank, orderId, setOrderId, time, setTime,language, setLanguage,realQuantity, setRealQuantity]}>
      <Router>
        <Navbar />
        <Switch>
          <Route exact path='/'>
            <Box className={classes.card}>
              <BuyFrontPage />
            </Box>
          </Route>
          <Route path='/buy-secondPage'>
            <Box className={classes.card}>
              <BuySecondPage />
            </Box>
          </Route>
          <Route path='/buy-thirdPage'>
            <Box className={classes.card}>
              <BuyThirdPage />
            </Box>
          </Route>
          <Route path='/buy-fourthPage'>
            <Box className={classes.card}>
              <BuyFourthPage />
            </Box>
          </Route>
          <Route path='/buy-finalPage'>
            <Box className={classes.card}>
              <BuyFinalPage />
            </Box>
          </Route>
          <Route path='/buyPdf'>
            <BuyPdf />
          </Route>
          <Route path='/sell-frontPage'>
            <Box className={classes.card}>
              <SellFrontPage />
            </Box>
          </Route>
          <Route path='/sell-secondPage'>
            <Box className={classes.card}>
              <SellSecondPage />
            </Box>
          </Route>
          <Route path='/sell-thirdPage'>
            <Box className={classes.card}>
              <SellThirdPage />
            </Box>
          </Route>
          <Route path='/sell-fourthPage'>
            <Box className={classes.card}>
              <SellFourthPage />
            </Box>
          </Route>
          <Route path='/sell-finalPage'>
            <Box className={classes.card}>
              <SellFinalPage />
            </Box>
          </Route>
          <Route path='/sellPdf'>
            <SellPdf />
          </Route>
        </Switch>
      </Router>
    </UserContext.Provider>
  );
}

export default App;
