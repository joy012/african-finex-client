import i18n from "i18next";
import { initReactI18next  } from "react-i18next";

// import Backend from 'i18next-http-backend';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

// the translations
import translationEN from './locales/en/translation.json';
import translationPT from './locales/pt/translation.json';

const resources = {
  en: {
    translation: translationEN
  },
  pt: {
    translation: translationPT
  }
};

// const Languages = ["en", "pt", "fr" ];

const options = {
  // order and from where user language should be detected
  order: ['querystring', 'cookie', 'localStorage', 'sessionStorage', 'navigator', 'htmlTag', 'path', 'subdomain'],

  // keys or params to lookup language from
  lookupQuerystring: 'lng',
  lookupCookie: 'i18next',
  lookupLocalStorage: 'i18nextLng',
  lookupSessionStorage: 'i18nextLng',
  lookupFromPathIndex: 0,
  lookupFromSubdomainIndex: 0,

  // cache user language on
  caches: ['localStorage', 'cookie'],
  excludeCacheFor: ['cimode'], // languages to not persist (cookie, localStorage)

  // optional expire and domain for set cookie
  cookieMinutes: 10,
  cookieDomain: 'myDomain',

  // optional htmlTag with lang attribute, the default is:
  htmlTag: document.documentElement,

  // optional set cookie options, reference:[MDN Set-Cookie docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie)
  cookieOptions: { path: '/', sameSite: 'strict' }
}

let language = '';
const userLang = navigator.language || navigator.userLanguage;
if (userLang === "en-US") {
  language = "en"
} else if (userLang === "pt" || userLang === "pt-PT" || userLang === "pt-BR") {
  language = "pt"
}


i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next ) // passes i18n down to react-i18next
  .init({
    react: { 
      useSuspense: false //   <---- this will do the magic
    },
    resources,
    lng: language,
    fallbackLng: "en",
    dibug: true,
    // whitelist: Languages,

    saveMissing: true,
    detection: options,

    //keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

  export default i18n;