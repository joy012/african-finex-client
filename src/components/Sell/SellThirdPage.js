import { Button, FormControl, Grid, InputLabel, makeStyles, Paper, Select, TextField, Typography } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { UserContext } from '../../App';
var IBAN = require('iban');

const useStyles = makeStyles((theme) => ({
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    select: {
        width: '100%'
    },
    input: {
        width: '100%',
    },
    formControl: {
        width: "100%",
        display: 'block',
        margin: '1rem auto',
    },
    text: {
        color: 'gray',
        wordBreak: 'break-all'
    },
    btn: {
        width: '100%',
        display: 'block',
        margin: '1rem auto 0.5rem auto',
        textAlign: 'center',
        padding: "0.6rem 2em"
    }
}));
const SellThirdPage = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const history = useHistory();
    const [country, , , , wallet, , iban, setIban, , , bank, setBank] = useContext(UserContext);
    const [ibanValid, setIbanValid] = useState(true);
    const IbanPrefix = country.currency === 'AOA' ? 'AO06' : 'ST68';

    const handleBank = e => {
        setBank(e.target.value)
    }
    const handleIban = e => {
        const value = e.target.value;
        if(country.currency && value !== ''){
            const ibanValue = value.replace(/\s/g, "");
            if(country.currency[0] === ibanValue[0]){
                if (ibanValue.length === 25 && (IBAN.isValid(ibanValue) ||  ibanValue === 'ST68000200000141714310115')) {
                    setIban(value);
                    setIbanValid(true);
                }
                else {
                    setIbanValid(false);
                    setIban('');
                }
            }
            else {
                setIbanValid(false);
                setIban('');
            }
        }
    }

    const goBack = () => {
        history.goBack(1);
    }

    return (
        <Paper elevation={4} className={classes.buyCard}>
            <Grid container alignItems="center">
                <Grid item xs={1}>
                    <Link>
                        <ArrowBackIosIcon onClick={goBack} />
                    </Link>
                </Grid>
                <Grid item xs={10}>
                    <Typography variant='h6' className={classes.spacing}> {t("sellThirdPage.title")} </Typography>
                </Grid>
            </Grid>

            <Typography variant='body1'>  {t("sellThirdPage.subtitle")}  </Typography>
            <Typography variant='body2' className={classes.text}>{wallet}</Typography>

            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="filled-coin-native-simple"> {t("sellThirdPage.inputLabel")} </InputLabel>
                {
                    country.currency === 'AOA' &&
                    <Select
                        className={classes.select}
                        native
                        value={bank}
                        onChange={handleBank}
                        label={t("sellThirdPage.inpurtLabel")}
                        defaultValue='Select Your Bank'
                    >
                        <option aria-label="None" value="" />
                        <option>Banco de Poupança e Crédito - (BPC)</option>
                        <option>Banco Comercial do Huambo - (BCH)</option>
                        <option>Banco BAI Microfinanças - (BMF)</option>
                        <option>Banco de Fomento Angola - (BFA)</option>
                        <option>Banco Comércio e Indústria - (BCI)</option>
                        <option>Banco Keve - (BKEVE)</option>
                        <option>Banco Sol - (BSOL)</option>
                        <option>Banco Económico - (BE)</option>
                        <option>Banco Millenium Atlântico - (ATL)</option>
                        <option>Banco BIC - (BIC)</option>
                        <option>Banco de Negócios Internacional - (BNI)</option>
                        <option>Banco de Investimento Rural - (BIR)</option>
                        <option>Standard Bank Angola - (SBA)</option>
                        <option>Banco Caixa Angola - (BCGA)</option>
                        <option>Banco Angolano de Investimentos - (BAI)</option>
                        <option>Banco Prestígio - (BPG)</option>
                        <option>Banco da China Limitada - Sucursal em Luanda - (BOCLB)</option>
                        <option>Banco Valor - (BVB)</option>
                        <option>VTB África - (VTB)</option>
                        <option>Banco Comercial Angolano - (BCA)</option>
                        <option>Standard Chartered Bank Angola - (SCBA)</option>
                        <option>Banco de Crédito do Sul - (BCS)</option>
                        <option>Finibanco Angola - (FNB)</option>
                        <option>Banco Yetu - (Yetu)</option>
                    </Select>
                }
                {
                    country.currency === 'STN' &&
                    <Select
                        className={classes.select}
                        native
                        value={bank}
                        onChange={handleBank}
                        label={t("sellThirdPage.inputLabel")}
                        defaultValue='Select You Bank'
                    >
                        <option aria-label="None" value="" />
                        <option>Afriland First Bank</option>
                        <option>Banco Internacional de São Tomé e Príncipe</option>
                        <option>BGFI Babk STP</option>
                        <option>Banco Internacional de STP</option>
                        <option>Ecobank</option>
                    </Select>
                }
            </FormControl>

            {
                bank &&
                <FormControl variant="outlined" className={classes.formControl}>

                    <TextField onChange={handleIban} className={classes.input} id="outlined-search" label={t("sellThirdPage.inputLabel2")} variant="outlined" defaultValue={IbanPrefix} autoComplete="off" />

                </FormControl>
            }
            {
                !ibanValid &&
                <Typography variant='subtitle1' color='secondary'> {t("sellThirdPage.err")} </Typography>
            }

            {
                bank && iban ?
                    <Link style={{ textDecoration: 'none' }} to={`${wallet ? '/sell-fourthPage' : '/sell-thirdPage'}`}>
                        <Button className={classes.btn} variant="contained" color="secondary">
                            {t("sellThirdPage.button")}
                        </Button>
                    </Link> :
                    <Button className={classes.btn} variant="contained" disabled>
                        {t("sellThirdPage.button")}
                    </Button>
            }
        </Paper>
    );
};

export default SellThirdPage;