import { Button, FormControl, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { UserContext } from '../../App';
import { useWallet } from 'use-wallet';

const useStyles = makeStyles((theme) => ({
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    textRight: {
        textAlign: 'right'
    },
    circle: {
        width: '12px',
        height: '12px',
        borderRadius: '50%',
        display: 'inline-block',
        marginRight: '0.2rem'
    },
    indicator: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end' ,
        textAlign: 'right',
        paddingTop: '1rem',
        cursor: "pointer"
    },
    formControl: {
        width: "100%",
        display: 'block',
        margin: '0.7rem auto',
    },
    input: {
        width: '100%',
    },
    btn: {
        width: '100%',
        display: 'block',
        margin: '1rem auto 0.5rem auto',
        textAlign: 'center',
        padding: "0.6rem 2em"
    }
}));

const SellSecondPage = () => {
    const { t } = useTranslation();
    let { account, connect, reset, status } = useWallet()
    const classes = useStyles();
    const history = useHistory();
    const [country, , , , wallet, setWallet, , , , , , , , , , , language] = useContext(UserContext);
    const [isWallet, setIsWallet] = useState(true);
    const [error, setError] = useState(true);
    const article = (language === 'pt-PT' || language === 'pt-BR' || language === 'pt')? 'https://africaswap.medium.com/como-instalar-a-binance-smart-chain-wallet-em-5-passos-1f23e833f548' : 'https://africaswap.medium.com/how-to-install-binance-smart-chain-wallet-in-5-steps-d7994f39dd15';

    const validateWallet = event => {
        if (/^0x[a-fA-F0-9]{40}$/g.test(event.target.value)) {
            setWallet(event.target.value);
            setError(false)
        }
        else {
            setWallet("")
            setError("showError")
        }
    }

    const handleBscWallet = () => {
        status === 'disconnected' && connect('bsc')
        setError(false)
        setWallet(account)
        setIsWallet(true)
    }

    useEffect(()=> {
        setWallet(account)
        setIsWallet(!isWallet)
    },[account])

    const handleClickWalletAccount = (e) => {
        setIsWallet(false)
    }

    const goBack = () => {
        history.goBack(1);
        reset()
        setWallet("")
    }

    return (
        <Paper elevation={4} className={classes.buyCard}>
           <Grid container alignItems="center">
                <Grid item xs={1}>
                    <Link>
                        <ArrowBackIosIcon onClick={goBack} />
                    </Link>
                </Grid>
                <Grid item xs={10}>
                    <Typography variant='h6' className={classes.spacing}> {t("sellSecondPage.title")} </Typography>
                </Grid>
            </Grid>

            <Typography variant='body1' className={classes.textLeft}> {t("sellSecondPage.desc")} </Typography>

            <Typography variant='body1' className={classes.textLeft}>  {t("sellSecondPage.desc2")} </Typography>

            <Typography variant='body2' className={classes.indicator} onClick={() => handleBscWallet()}><span className={classes.circle} style={{backgroundColor:error?"red": (account || wallet) ? "green" : "red"}}></span> {t("buySecondPage.binance")} </Typography>

            <FormControl variant="outlined" className={classes.formControl}>
                { isWallet && <TextField onChange={validateWallet}   onClick={e => handleClickWalletAccount(e)} inputProps={{ "data-shrink":account?"true":"false" }} defaultValue={wallet} value={wallet}  className={classes.input} id="outlined-search" label={wallet && t("buySecondPage.inputLabel")}  variant="outlined" autoComplete="off" />}

                { !isWallet && <TextField defaultValue={wallet} autoFocus={!isWallet && true} onChange={(event) => validateWallet(event)}  className={classes.input} id="outlined-search" label={ t("buySecondPage.inputLabel")}  variant="outlined" autoComplete="off" />}
            </FormControl>

            <Grid container spacing={1}>
                <Grid item xs={6}>
                    <Typography variant='subtitle2'>{country.trueCurrency}</Typography>
                </Grid>
                <Grid item xs={6}>
                    {
                        error === "showError" &&
                        <Typography variant='subtitle2' color='secondary' className={classes.textRight}> {t("buySecondPage.err")} </Typography>
                    }
                </Grid>
            </Grid>

            

            

            <a href={article} target='_blank' rel="noreferrer">
                <Typography color="primary" className={classes.spacing}> {t("sellSecondPage.link")} </Typography>
            </a>


            {
                wallet ?
                    <Link style={{ textDecoration: 'none' }} to={`${wallet ? '/sell-thirdPage' : '/sell-secondPage'}`}>
                        <Button className={classes.btn} variant="contained" color="secondary">
                            {t("sellSecondPage.button")}
                        </Button>
                    </Link> :
                    <Button className={classes.btn} variant="contained" disabled>
                        {t("sellSecondPage.button")}
                    </Button>
            }
        </Paper>
    );
};

export default SellSecondPage;