import { Button, CircularProgress, FormControl, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import React, { createRef, useContext, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import ReCAPTCHA from "react-google-recaptcha";
import { UserContext } from '../../App';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    formControl: {
        width: "100%",
        display: 'block',
        margin: '0.5rem auto',
    },
    input: {
        width: '100%',
    },
    text: {
        marginTop: '0.3rem',
        fontSize: '1.05rem'
       
    },
    wordWrap: {
        wordBreak: 'break-all'
    },
    articleLink: {
        textAlign: 'right',
        margin: '0.5rem auto'
    },
    btn: {
        width: '100%',
        display: 'block',
        margin: '1rem auto 0.5rem auto',
        textAlign: 'center',
        padding: "0.5rem 2em"
    },
    captcha: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '0.5rem auto'
    }
}));

const SellFourthPage = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const history = useHistory();
    const [country, , quantity, , wallet, , iban, , TXid, setTXid, bank, , order, setOrder, time, setTime, language] = useContext(UserContext);
    const [captcha, setCaptcha] = useState('');
    const [isValid, setIsValid] = useState(true);
    const [dbInitiate, SetDbInitiate] = useState(true);
    const recaptchaRef = createRef();
    const data = {
        country: country,
        currency: country.currency,
        coinQuantity: quantity,
        wallet: wallet,
        Bank: bank,
        IBAN: iban,
        orderId: order,
        TXid: TXid,
        timeStamp: time
    };
    const article = language === 'pt'? 'https://africaswap.medium.com/como-encontrar-o-transaction-id-4f467d5a3993' : 'https://africaswap.medium.com/how-to-find-the-transaction-id-6e8117246788';

    const handleAddSell = e => {
        if (captcha) {
            SetDbInitiate(false);
            fetch('https://african-finex-server.herokuapp.com/sellCoin', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .then(result => {
                    if (result) {
                        SetDbInitiate(true);
                        history.replace('/sell-finalPage');
                    }
                    else {
                        alert("For some Reason We can't connect to the server. Try Again Please")
                        history.push('/sell-frontPage')
                    }
                })
        }
        else {
            alert('Please Verify you are not a robot')
        }

    }

    const validateTx = event => {
        if (/^0x([A-Fa-f0-9]{64})$/.test(event.target.value)) {
            setTXid(event.target.value);
            setTime(new Date().toLocaleString());
            setOrder(
                country.currency === 'AOA' ? `AO${Math.floor(100000 + Math.random() * 900000)}` :
                    `ST${Math.floor(100000 + Math.random() * 900000)}`
            )
            setIsValid(true);
        }
        else {
            setIsValid(false);
        }
    }
    const handleCaptcha = () => {
        const recaptchaValue = recaptchaRef.current.getValue();
        setCaptcha(recaptchaValue);
    }

    const goBack = () => {
        history.goBack(1);
    }

    return (
        <Paper elevation={4} className={classes.buyCard}>
            <Grid container alignItems="center">
                <Grid item xs={1}>
                    <Link>
                        <ArrowBackIosIcon onClick={goBack} />
                    </Link>
                </Grid>
                <Grid item xs={10}>
                    <Typography variant='h6' className={classes.spacing}> {t("sellFourthPage.title")} </Typography>
                </Grid>
            </Grid>


            <Typography variant='body1'>{t("sellFourthPage.desc")} </Typography>

            <Typography variant='subtitle1' className={classes.text}> {t("sellFourthPage.subtitle1")}  </Typography>
            <Typography variant='body2' className={classes.wordWrap}>0xE2CF709Da0B42E5ACf05ebe35cB15c17f8Adc053</Typography>

            <a href={article} target='_blank' rel="noreferrer">
                <Typography color="primary" className={classes.articleLink}> {t("sellFourthPage.link")}  </Typography>
            </a>

            <FormControl variant="outlined" className={classes.formControl}>
                <TextField onChange={validateTx} className={classes.input} id="outlined-search" label={t("sellFourthPage.inputLabel")} variant="outlined" autoComplete="off" />
            </FormControl>
            {
                !isValid &&
                <Typography variant='subtitle1' color='secondary'> {t("sellFourthPage.err")}  </Typography>
            }
            <Typography variant='subtitle1'>{country.trueCurrency}</Typography>
            {
                TXid &&
                <ReCAPTCHA
                    ref={recaptchaRef}
                    className={classes.captcha}
                    sitekey="6LcSvVIaAAAAAI5CwgAlMiWNqVr4K8GU-LqHe-Xx"
                    onChange={handleCaptcha}
                />

            }

            {
                (TXid && captcha) ?
                    <Link style={{ textDecoration: 'none' }} to={`${TXid & captcha ? '/sell-finalPage' : '/sell-fourthPage'}`}>
                        {
                            dbInitiate &&
                            <Button onClick={handleAddSell} className={classes.btn} variant="contained" color="secondary">
                                {t("sellFourthPage.button")}
                            </Button>
                        }
                        {
                            !dbInitiate &&
                            <Button className={classes.btn} variant="contained" color="secondary">
                                <CircularProgress />
                            </Button>
                        }
                    </Link> :
                    <Button className={classes.btn} variant="contained" disabled>
                        {t("sellFourthPage.button")}
                    </Button>
            }
        </Paper>
    );
};

export default SellFourthPage;