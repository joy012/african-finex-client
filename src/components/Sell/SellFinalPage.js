import { Box, Button, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { UserContext } from '../../App';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
    textLeft: {
        margin: '0.6rem 0'
    },
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    text: {
        color: 'gray',
        wordBreak: 'break-all'
    },
    break: {
        margin: '-0.15rem auto',
        wordBreak: 'break-all'
    },
    thanks: {
        margin: '1.5rem auto 0rem auto',
        textAlign: 'center',
        fontSize: '1.2rem'
    },
    btn: {
        width: '100%',
        display: 'block',
        margin: '0.8rem auto',
        textAlign: 'center',
        padding: "0.6rem 2em"
    }
}));

const SellFinalPage = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const history = useHistory();
    const [country, , quantity, , , , iban, , TXid, , bank, , order, , time, , , , realQuantity] = useContext(UserContext);
   
    const download = () => {
        history.push('/sellPdf');
    }

    return (
        <Paper elevation={4} className={classes.buyCard}>
            <Typography variant='h5' className={classes.spacing}> {t("sellFinalPage.title")}  </Typography>

            <Typography variant='body1'> {t("sellFinalPage.subtitle")} </Typography>

            <Typography variant='body2' color='secondary' className={classes.textLeft}> {t("sellFinalPage.desc")}  </Typography>

            <Box>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("sellFinalPage.orderId")} :</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{order}</Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("sellFinalPage.IBAN")} :</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{iban}</Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("sellFinalPage.Bank")} :</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{bank}</Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("sellFinalPage.Txn")} :</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{TXid}</Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("sellFinalPage.Coin")} :</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{quantity} {country.currency}</Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("sellFinalPage.Time")} :</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{time}</Typography>
                    </Grid>
                </Grid>
            </Box>

            <Typography variant='subtitle2' className={classes.thanks}> {t("sellFinalPage.Thank")}  </Typography>

            <Button color='secondary' variant='contained' className={classes.btn} onClick={download}> {t("sellFinalPage.button")}  </Button>
        </Paper>
    );
};

export default SellFinalPage;