import React, { useContext, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { UserContext } from '../../App';
import logo from '../../image/pdf logo.svg';
import { useHistory } from 'react-router-dom';
import './BuyPdf.css';
import jsPDF from 'jspdf'
import 'jspdf-autotable';



const BuyPdf = () => {
    const [country, setCountry, quantity, setQuantity, , setWallet, iban, setIban, , , bank, setBank, order, setOrder, time, setTime, language, , realQuantity, setRealQuantity] = useContext(UserContext);
    const { t } = useTranslation();
    const history = useHistory();
    const partnerIban = country.currency === 'AOA' ? 'AO06 0067 0000 0026 0933 1028 0' : 'ST68 0002 0000 0141 7143 1011 5';
    const partnerBank = country.currency === 'AOA' ? 'Banco de Investimento Rural - (BIR)' : 'Banco Internacional de São Tomé e Príncipe';
    const buyingFee = country.currency === 'AOA' ? '5000' : '150';

    const printDocument = () => {
        const pdf = new jsPDF("p", "mm", "a4");

        const myImg = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAAAaCAYAAAAwnlc+AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAB3RJTUUH5QIPFRMk7/+nfwAAEyZJREFUeNrtm3mQZVV9xz/fc+97PdMzPcO+jEjvICBGQYKKAiLiMoAiQShwwQomUYISxIpapSBBE9xjiCQhQVxSmkQkuIBAFEEglKyiIDAzvRBA9oGZoae7373nmz/OeW+a6Z5hlGFIqvxVvXrVr88953fO+S3f33L1iWrpLpK/CjrUNpLYUmSMEMD9tk9C+hFQyI6fLC/1pszRO7gbVFEISwrtOcdGH4nDe21Pay0aG1nWmat3YEgzFi5kAbaVvkER0PiMZ2Y8i2gPdJAl4yhkQGOjyzaJ52dDA4PD1E5MC4JRgR3TtQkg2gYRhEDUCBFhbOS55++3pVLifJtDwVFS2JKLC2FszAskXQi8BHiYxMcmHZZiFAEDSgKUpKh3YHtNr336HL0Dw4IkfMIyqpwkyuMjy2YOnXPt8ZHlM/8X1/v3FrncOibhI2mKhVsGjY8sX3/9ekvz9rtQCbwOFCW2qPC1SUjIFbAj8IfAD4DA7AueRbv2DYCEawtjFdoWe4HEpEseKe9D1Y6mf3CY0RVZwGxRzsP1WgHHA0cZ7dg7MLw98GXgPKAcH1lWrb9e78CQsgWMxp+V9TrjE4VuB4qx0WX1M/H8bKh3YJjuMOW1sRmAKNgJeL/QK/r6hxchCsNRsgPo28ANhtMEQmZstpA+71QC9wv3Gap8uFuabKtIK/u+9m+b8qBUyBhKDGoQuRLxMvB9VOxX76QHhUK085xGKFBN1pbOEXzIqMZeCWyN2GbjnIKUPDjwsvzZZkvd6vjIMvr6h9rKuRjpx8Z7yp4EngIKocKwSPiViHLdhT4fV/vMVNqcjvR1QbefHzYtWVhfsrjNEIqEw56RJGMrYGrgYMsvE6qMdwEdYft8IEih7u0fBpChRnoB8H7MauCQFtzSVGyAKwBTxr6B4YBwPTVF0eySIWBjO+ZD+hViCWblzDPr7R/SfJZ7SgOKDkIoYUWczC+WZJOBo4JxlEHYkqQs4JGkiPlakoaCC0xEHGd7T0n/CX4HuAUEW5NCewH3GH7VeQrTlyCIAEfbyTJKQMiLWmldRcz4aLKY/YPDxBmuHwgkRgFiDEp7M/I8WS2jmrQn1m011tFFEWbOo1LiIsNtwKuxw5Y0gjnoCcDdRVFd16oaCkV03AST0jc4nI/KypJ1fMLmXCZ0hOUTQOeni2zLjHLMQy8w3/BjyTc1obCL6fHRZe4b3A1iNDlUCY2uUBZ1vWj+lB9d1c34WAfInzqTn/7+gbp3YEiAJxkKRiFM3F/FxbtBvVbExCwiZO1KZjnWIcEQaoJEZRQgr1/kmKcmYdZsfoXhxTkaugT0FKgcH1k2ndm5A9h95lEnHoeIaf9WClJkuwaM1MaVxWSjFRt14Qw5qKPJilOka3Od5Q3AIcb0O0SttYoQHDFJmBWcPVAIEukPZewdS3AhGAFWBKEzix9uMZxwZjwcx5hOOJYhyCbKHTHZmPBGS8kIVsD2wFuRJ2x9GHgRcJDE3sAvSW6pxhRh7UTt7u6tsw7WVffCUK6dWChR9w0OT/TUO8dVxQM9oIC9RnJV1cUOj63p3l1y6Osf+rmltYIeSw3ZqwxVzJYFCNnhV7F7yVZUa4axFgNrwCO19bBAQVF10SRU0zYxQrGI6AEKbeNkiccasbi3KupkoZ5aE92zGGJsemqqUlfX4o4hmZpC8+ct7BvcbSZubRhXBWG1sZwCtEKoB4hEr7FcI/UDAzaTgjsCfmJ+qxGyQCWJRI5yMfqeU2PvBZ+XCENIL8xWc6QxVd3b6ioDUsCOsY4iaFtLFXhtYaZrSbatMii2IiGoAPXozHopEYIgbO40jJ9RlI0kgSPrXI6EfGbxww0+NTg4SB0lRAFUWO9GXGj7EqG3Wv6i0KnYZxmdAXQBU8AfIG4VROwieUYqoAE8JNiDplZ6yjchesH7CU4Gne68IaF9EbcYXyR0lM2rlMB+IdlO+9hacIbNexCL5ORys/m6TOhYYHV2tXsg/h70KuyuhDFBOBpdITjFZjmiCUwLPm34KNCS3UCKfnrAJmAq+75rBG8mYf0KGAZuAW60fDTWN4ClQJQJwGOW/0LmG6AQZSulrAqgFjrY9ueAfZGSCRQYvif7FMO9QAOpBXwcOAu4GPsYIedjKIBK+NNGp5XRSBA6BnWzCJ6zh8exIiahngNhOoGO9ExKygC0yo3P34ophSMcQVicoKSv305ejovAp1q8HfjUxMTa6e7ueQArZZ2H2NXicMx9gkuA+cCTiCnfFaV+tjf0AOdjHYr4LvbtSC/CtDKbW2Xum+nWJaMa2AZ8hWFf0EPCFxqWA4tBByMOBu8AWp2B4b6yXmt8KdJNwGPAtima5Y3A5YJXYB7J4nWzzAXAKy32kLlKcCcwLx/PNLCT8dGaHSCVgoUWS2RdbrOH4IvAQ4aDkN8E+rrFOOYaWUW+nho4DvhWgqi+BPsaSwXmSMGRwB7AgYIHY4xI+hxwBHAU6GOGv8KaJzFpeynio7JX64x6acgaJIE/uRHL81zQJ6aXJuns5KSTpJ7V3DAU6BsYlpPG1sBeiF/JPGjYE1hJrINCcaPFPsAbgCuAIiP5CjgYuMr4UsFSUGE7UmMiJQ3dKTFsswp4A+IGIMi2FIJxbXy5rMOMDwT9DGgipoX/ydZ7JV9veAvwKMnD5Byl+sGPJgE0QF+KMTwGyBY7HbySB65aRKHwNcQ7sc5wsiZlRv4V6ALs9xi9XfAfJCvnfCb7ADcjrgcOYJ0F3BP4Rc4b/DdwGLBG+f/G5wqdbPkbMu+yaAAt0K7gXwBbCY60+T5QSAn3Yb6MOAXzFUknA00nRdhLcAN4oc1hwJVAL+h6iSXAiWVrUjS6/F7gcIPPqJc+K4EydkZxkzYXx8rXKqjbdtRM/97xM5Zg0tZDsluAnmi0Nih8Q0NDVCnbloIP+eiM2C+WWAma5xAmLf4N2Af7eElXzFgTTDMLe8jJ8BKIslp5mZgW8OeQbrDpkog5km27u85eUlnC05hdEScgbOsDiEcxXYJogUxEHiX5NxRqgPHMGSMjI07zDRWFqIFPYd5p/EaJs4DapqEkzSEz0MznXgIWqo2bWZtnOzVTICPpNCdc2kWKhytZFyBOlrWvUDAxpnn8TtBW2OcifV9QBocY1wXNZwMnAsfZfAx4EtxA3AE6xeir4H8A9hf6guUlts5F/lpZdvmzhtMke3PkAduONBuzY0PBn8fINRILmYlV8koZmxYK9MxvdN9VUXnrqhOqz6JWleskogKato7NYPOiXKFqR/+XAH+DdKRTkvshJY1O5al8HQmHqQKik0+0UJGMMj+WjaC6f16sl0wGYcLsa+1giZfb7kZcHxxvNiocwrQdWVDVPFWUgALYMUQgSJZkOQb39fUPv9xiF5t5EtFmYTKO7IrdxTpsl6EXCOr0t6r2fpSgwBzkNuj+H/Av8zam25kEi/sEU0Y7mtgtsyadiQ7JSZUrQxR1qJt1qCsAWY3t1ix++PEFq25Dfg1WX7KWiuASfCHWQZJOxL4BMQj6pcVfRgVKiT/NkXE9p8b8LkKYTqmW1DQcgfmvLDAbyu9VQM/a1toeYNXG5xa2k/s1B0na03B7ND9VQsXTwgVwN9L3sY80Wgq+ABQ2tkMh2hkDp9TbI1kLvGQyGxzNVgx3KsS8MOvwshgCYCkaY99572hneG//sKROXq3H8rnAO1Kgwgzl7MRxBagBTKXV1uf76XvYCDnrymq2m5jgyXnQCvkZIahsV5KLZAFBCVr0Zqn/VgzRIilNXtqPLXwSTDPzvmPHJ5g6TrcIjcbJhv0l7WG8GjhBMFFEF2WORDeH3M3a67rMxPrHtJFhG6H+gaGUogu5boJPyBNsFwJXkqJZ50zatExfnvsdEhewCeW99RjqpDXGR5Y75/lmURvEAo0sAJWyfbLM+MiKp49PCfR0S/J5RsdnTPa3wD3gCawpw/aI64DCyOvWe3aZMgH++WHWnj+FKqx/9jNuw5DKoo1czfspsJJ1eHPmU9OYBnB/3mQEh9Bs1uBdge3yyJDHAVDa+keSCy7YTFnolHNQYRvM95CamelZApCdVwGs7JnsXt0qKqYarTnnjZaUfHwF7AQ6PO2V7YElc5x0lcObg4xeAtyudfhgs1EngpIfThvykiyIFqk6sm67ZK/uCPRhjkP8BnEk8KjpdLG0c8YNt33kZuwpEE9syDR0NFCI6UqTjTI+Cuxq+GvgWqAY30Ddu7d/kFAGxcruWtTl6dVTXYYLhbbHvk7oAItvAvuDVpfTtT7cLHyX0dKZ6O1Z30aqT3431rpOgW7bE1rP1NrOYzUZoh5a05wwQj3NVXOedHZzgQTqlwq2xb4BdHTOVc2kILPKcJ7gWMPRgts32w3Ofa235+PbH3knw4OChlJkqgwBZkKdnYEgfJetRxHdQItkySeAV2XX2Hr2F7M+TT/jhDtvLe5/3AC3IPYBHy50re15vQNDFZ1QMt8iatk41rKgnF41XVk6W2Z/4ysEbzBcJHib4TOy31d2lRGs87H/GT1PaRiltDAxbWT11KJZTrm3fyhb1hyFWschY/Ed4AGgDDHWo2MrUm5jYKh0wp3/ChwLPsZwjqSJzX+ZwqYE3QbxJ5IOsf0RSaeSggQnZXNNpEidR6pJQoagX7gn16YDSeAWGE7PoHAufp/LipUBHn4ylaJtvgmchHif8b8jbgFCkFxHuHe06X1eupbHVhXbIR7HLnI1561YpyM/LPR+ANkfsNkP8WcWN5fRSVrbCPvZpmGgU+PFtmO9kUQ0EFNlU9ntGFBrevbhjo8uT50gqfHgxeDXkioDP+wY3RCUC+60Gw2Bq4XvRewhOAi4LF8ybBgTbkozRHtM2z0GUsHrI8ZXAR+0vUTwd4LfSHSDXm7xLvAfg1cg34F1G/BSwwVIZwNPCIYNXwDvaJhIjaednP3ThGRjAvQ77q9dlaKOIKkEXw18ydapyFfIfAZxhc1qiZ7e/undHn+yOAl5QWkOaElRKXf4FaUWh5OBFYguxP3AnwhdZvz5kFowiGgmtM1X59ReO1216lZdbfKninX7O0bV1FTUtL9nfFxRq0WtGsoWLlvURYuuntkYsLd/KHVuSBidkDAmVxvuygJVY8exkWURK+a8YylpFegHOcI9MU/XvsvZNZcE3sr1xs1FxcxvQw0qhG6UeRNoBXAM4mpgGeg2i38B701qnQKHCjjZ6HFJfwTcCowCVwovwDqO5L6bc7CzMR7DemPW/z1w8qWaJdJ09j4zWxANYWpi3mnAx4W6jc4B3Uqq8NwKfBv8etA9NRBMU+brWDsDX0L+DqJETGGVoB8ZnyNrkc6olxZK7U8GdGbxgy3WjPChNW+hp/tJ2fJUq0uNUOfOIensrivWd8FtCGnb+0va2fjXsu5OB2bPbLjsHRiW2n2AYhdgX8wTSFcD2wKvNr5PcDPk/qgau7bU1GssLwCuJrlJtaPgdnBhvJ+snY1/JrSSdhbHLlIKigXAoUL7I2/tFD3+ArhK6GFIACLVpbXE+G2SXgRUhpuAi42nBYfkBqerheu81wjaG3sIdAPwmyxc7fatxcCBiEeAG+hEtl6IdSDyKkZ2u5Y974SpMguiDDRsHyw5Cl1NygIo5tQUhFryC2xeL/xiJ8z6ONavBddPtaqxZqOwxDag12DW5vVXtRUlXZASL/BKfaJ6M5KGgQM6eSgbp36vKjretLY1Nd4sG+1qxmYRPpNiO6fGnMkdyvKpsTVTLF5QMlXVnNO8coOK0Ns/PMOnd4TsaW3pfSnydPtk80CNjT43XcFJQTpbKzKciIDGRpZ7j70H20MFsGZVIBQ4Xaw6fX/t0jjtrpo8B44eH12xyfxsDkp9gO2IGBmHiGL7LO8dXeZd+oYpQ+poyqklSK1b+bf19peL9gl1OZaSjsF8DTF/XW1JnesVmp7X6PpgFavrgsKC/BLOs97cujmEbB5ptR6Y1xXum6qimnPM39s/iBQ6m8qNm1FOLxGt33sjyalUhJWqF4WN+waG21Fo4dQgWs9hAUOCxtTtg5vDAhb5paQ6v5SU+koV03OWwCWS+vqHPbGm020RQ1AsGlZ6lUhRJliUKS2Q+xrsOtVKKFM3TRH7+ofdObD0MpJSxSEL6TrhFXSUoL3fBCOtAtmM7FbPYQFlu8gJ/ZpsosGqY8CEugwx2BSA+vqHU7epg1NjCE7RMDIUqXiizhmOjS7zDOMBVqkz6qXjtndFs1vys8Vr2L61VVfvVsrnbXYLMqMx9U6yy/t08/ItBgU2N+2+++5MTtftFn7l2ivOEtZVBu655x4A+gaGSE3WEdTIbioJdKnIihXLn+/tdGhwcJCqzioY2h3dqXwiw6Luedxxxx2/1ZwleEnGgHM1QbVB6w6Sum1P67d4Y21TKClJR4Ob5NTE/2e6++6719vihmlsZPkmj32+acWKp0GAzcJrwPqJcZi7fbRT/rkxOj6ZQvLNRnLb2ZsSmA4Ka1Kp47koDf6e/i9SMD4J8RPceRkGaJfTHGxfV8X6C0Jd+efNpaVWwnOSNCkxGqmrSNT8suv5Ppff0xai/wU0WpafQ19F7QAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMS0wMi0xNVQyMToxOToxNiswMDowMDzmzhAAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjEtMDItMTVUMjE6MTk6MTYrMDA6MDBNu3asAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAABJRU5ErkJggg==`;

        pdf.addImage(myImg, 'PNG', 80, 15)

        pdf.setFont('Roboto-Light', 'normal');
        pdf.setFontSize(20);

        if (language === 'pt') {
            pdf.text(`${t("pdf.title1")}T${country.currency}${t("pdf.title2")}`, 35, 40, { maxWidth: 190, align: "justify" });
        }
        else {
            pdf.text(`${t("pdf.title1")}T${country.currency}${t("pdf.title2")}`, 44, 40, { maxWidth: 190, align: "justify" });
        }

        pdf.setFontSize(14);

        pdf.text(`${t("pdf.subtitleP11")}T${country.currency}${t("pdf.subtitleP12")}`, 28, 52, { maxWidth: 155, align: "justify" });

        pdf.autoTable({
            html: '#simple_table',
            tableLineColor: [255, 255, 255],
            tableWidth: 130,
            body: [
                [`${t("buyFinalPage.orderId")}`, `${order}`],
                [`${t("pdf.amount")}`, `${realQuantity} ${country.currency}`],
                [`${t("buypdf.IBAN")}`, partnerIban],
                [`${t("buyFinalPage.Bank")}`, `${partnerBank}`],
                [`${t("buyFinalPage.Time")}`, `${time}`],
            ],
            bodyStyles: {
                lineWidth: 0,
                lineColor: [255, 255, 255]
            },
            margin: { horizontal: 45 },
            startY: 70,
            theme: 'grid',
            columnStyles: {
                0: {
                    cellWidth: 30,
                },
                1: {
                    cellWidth: 90,
                },
            },
            styles: {
                minCellHeight: 6,
                fontSize: 12,
                textColor: 'black',
                overflow: 'linebreak'
            }
        });

        pdf.text(`${t("pdf.subtitleP21")} ${iban}${t("pdf.subtitleP22")} ${country.currency} ${buyingFee} ${t("pdf.subtitleP23")}`, 28, 122, { maxWidth: 155, align: "justify" });


        if (language === 'pt') {
            pdf.text(`${t("pdf.subtitleP31")}${quantity} T${country.currency}${t("pdf.subtitleP32")}`, 28, 155, { maxWidth: 155, align: "justify" });
            pdf.text(`${t("pdf.subtitle4")} ${order}`, 28, 178, { maxWidth: 155, align: "justify" });
        }
        else {
            pdf.text(`${t("pdf.subtitleP31")}${quantity} T${country.currency}${t("pdf.subtitleP32")}`, 28, 150, { maxWidth: 155, align: "justify" });
            pdf.text(`${t("pdf.subtitle4")} ${order}`, 28, 166, { maxWidth: 155, align: "justify" });
        }

        pdf.save("AfricaFinexBuy.pdf");

        setCountry({});
        setQuantity(0);
        setWallet('');
        setIban('');
        setBank('');
        setOrder('');
        setTime('');
        setRealQuantity(0);
        history.replace('/')
    }

    useEffect(() => {
        printDocument()
    }, [])

    return (
        <>
            <div id='divToPrint' className='box'>
                <img className='logo' src={logo} alt="" />
                <h1 className='title'>{t("pdf.title1")}<span className='bold'>T{country.currency}</span>{t("pdf.title2")}</h1>

                <p className='spcaing'>{t("pdf.subtitleP11")}<span className='bold'>{country.currency}</span>{t("pdf.subtitleP12")}</p>

                <table id="simple_table" class='table'>
                    <tr>
                        <td>{t("buyFinalPage.orderId")}</td>
                        <td>{order}</td>
                    </tr>
                    <tr>
                        <td>{t("pdf.amount")}</td>
                        <td>{realQuantity} {country.currency}</td>
                    </tr>
                    <tr>
                        <td>{t("pdf.IBAN")}</td>
                        <td>{partnerIban}</td>
                    </tr>
                    <tr>
                        <td>{t("buyFinalPage.Bank")}</td>
                        <td>{partnerBank}</td>
                    </tr>
                    <tr>
                        <td>{t("buyFinalPage.Time")}</td>
                        <td>{time}</td>
                    </tr>
                </table>
                <p className='spcaing'>{t("pdf.subtitleP21")}<span className='bold'>{iban}</span>{t("pdf.subtitleP22")}{country.currency}{t("pdf.subtitleP23")}</p>

                <p className='spcaing'>{t("pdf.subtitleP31")}{quantity} T{country.currency}{t("pdf.subtitleP32")}</p>
                <p className='spcaing'>{t("pdf.subtitle4")} <span className='bold'>{order}</span></p>
            </div>
        </>
    );
};

export default BuyPdf;