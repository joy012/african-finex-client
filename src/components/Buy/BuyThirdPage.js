import React, { useContext } from 'react';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import { Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import { UserContext } from '../../App';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    paper: {
        padding: '0.5rem',
        margin: '1rem auto',
        maxWidth: 300,
        backgroundColor: '#f4f7fc'
    },
    bankLogo: {
        margin: 'auto',
        display: 'block',
        fontSize: '3.2rem'
    },
    lineGap: {
        lineHeight: '1.7rem'
    }
}));


const BuyThirdPage = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const history = useHistory();
    const [country, , ,] = useContext(UserContext)

    const goBack = () => {
        history.goBack(1);
    }

    return (
        <Paper elevation={4} className={classes.buyCard}>
             <Grid container alignItems="center">
                <Grid item xs={1}>
                    <Link>
                        <ArrowBackIosIcon onClick={goBack} />
                    </Link>
                </Grid>
                <Grid item xs={10}>
                    <Typography variant='h6' className={classes.spacing}> {t("buyThirdPage.title")} </Typography>
                </Grid>
            </Grid>
            {
                country.currency &&
                <Link style={{ textDecoration: 'none' }} to='/buy-fourthPage'>
                    <Paper className={classes.paper}>
                        <Grid container alignItems="center" spacing={1}>
                            <Grid item xs={3}>
                                <AccountBalanceIcon className={classes.bankLogo} />
                            </Grid>
                            <Grid item xs={9} sm container>
                                <Typography variant='h6' className={classes.lineGap}>{country.currency} {t("buyThirdPage.bankForAOA.bank1.title")} </Typography>
                                <Typography variant='body1'>  {t("buyThirdPage.bankForAOA.bank1.desc")} {country.currency} </Typography>
                            </Grid>
                        </Grid>
                    </Paper>
                </Link>
            }
            {
                country.currency === 'STN' &&
                <Link style={{ textDecoration: 'none' }} to='/buy-fourthPage'>
                    <Paper className={classes.paper}>
                        <Grid container justifyContent='spcae-evenly' alignItems="center"  spacing={1}>
                            <Grid item xs={3}>
                                    <LocalAtmIcon className={classes.bankLogo} />
                            </Grid>
                            <Grid item xs={9} sm container>
                                <Typography variant='h6' className={classes.lineGap}>{t("buyThirdPage.bankForSTN.bank2.title")}  </Typography>
                                <Typography variant='body1'>  {t("buyThirdPage.bankForSTN.bank2.desc")} </Typography>
                            </Grid>
                        </Grid>
                    </Paper>
                </Link>
            }
        </Paper>
    );
};

export default BuyThirdPage;