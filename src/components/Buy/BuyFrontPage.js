import { Button, FormControl, Grid, InputLabel, makeStyles, Paper, Select, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { useState } from 'react';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from "react-i18next";

import { UserContext } from '../../App';

const useStyles = makeStyles((theme) => ({
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    formControl: {
        width: "100%",
        display: 'block',
        margin: '1.2rem auto',
    },
    select: {
        width: '100%'
    },
    input: {
        width: '100%',
    },
    textLeft: {
        textAlign: 'left',
    },
    textRight: {
        textAlign: 'right',
    },
    btn: {
        width: '100%',
        display: 'block',
        margin: '1rem auto 0.5rem auto',
        textAlign: 'center',
    },
    bold: {
        fontWeight: 'bold'
    }
}));

const BuyFrontPage = () => {
    const classes = useStyles();
    const [country, setCountry, quantity, setQuantity, , , , , , , , , , , , , , , realQuantity, setRealQuantity] = useContext(UserContext)
    const [coin, setCoin] = useState('');

    const { t } = useTranslation();

    const handleCountry = event => {
        if (event.target.value.split(' ')[0] === 'TAOA') {
            setCountry({
                name: 'Angola',
                currency: 'AOA',
                trueCurrency: event.target.value
            });
            setCoin(event.target.value);
        }
        else {
            setCountry(
                {
                    name: 'São Tomé and Príncipe',
                    currency: 'STN',
                    trueCurrency: event.target.value
                }
            );
            setCoin(event.target.value);
        }
    }

    const handleQuantity = event => {
        const amount = event.target.value;
        if (amount > 0) {
            setQuantity((amount - (amount * 0.02)).toFixed(2));
            setRealQuantity(amount);
        }
        else if (amount <= 0) {
            alert("Invalid Coin Value.");
            event.target.value = 0;
        }
    }


    return (
        <Paper elevation={4} className={classes.buyCard}>
            <Typography variant='h6' className={classes.spacing}>{t("buyFrontPage.title")}</Typography>
            <Link style={{ textDecoration: 'none' }} to='/sell-frontPage'>
                <Button color='primary'>{t("buyFrontPage.link")}</Button>
            </Link>

            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="filled-coin-native-simple">{t("buyFrontPage.selectLabel")}</InputLabel>
                <Select
                    className={classes.select}
                    native
                    value={coin}
                    onChange={handleCountry}
                    label='currency'
                    inputProps={{
                        name: 'Currency',
                        id: 'outlined-coin-native-simple',
                    }}
                >
                    <option aria-label="None" value="" />
                    <option>TAOA - True Kwanza</option>
                    <option>TSTN - True Dobra</option>
                </Select>
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
                <TextField
                    className={classes.input}
                    id="outlined-number"
                    label={t("buyFrontPage.inputLabel")}
                    type="number"
                    onChange={handleQuantity}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                />
            </FormControl>



            <Grid container spacing={1}>
                <Grid item xs={9}>
                    <Typography variant='subtitle2' color='secondary'>{
                        (country.currency === 'AOA' && quantity !== 0 && realQuantity < 10000) ? t("buyFrontPage.err1") :
                            (country.currency === 'STN' && quantity !== 0 && realQuantity < 300) ? t("buyFrontPage.err2") : ''
                    }</Typography>
                    {
                        (country.currency === 'AOA' && quantity !== 0 && realQuantity >= 10000) || (country.currency === 'STN' && quantity !== 0 && realQuantity >= 300) ?
                            <Typography variant='body2' className={classes.textLeft}>
                                {t("buyFrontPage.buy")} <span className={classes.bold}>{quantity ? (quantity % 1 === 0 ? `${parseInt(quantity)}` : `${quantity}`) : ''} {country.name ? `T${country.currency}` : ''}</span> {t("buyFrontPage.for")} <span className={classes.bold}>{quantity ? realQuantity : ''} {country.name ? `${country.currency}` : ''}</span>
                            </Typography> :
                            ''
                    }
                </Grid>
                <Grid item xs={3}>
                    <Typography variant="body2" className={classes.textRight}>
                        {(country.currency === 'AOA' && quantity !== 0 && realQuantity >= 10000) || (country.currency === 'STN' && quantity !== 0 && realQuantity >= 300) ? `2% Fee` : ''}

                    </Typography>
                </Grid>
            </Grid>

            {
                (country.currency === 'AOA' && realQuantity >= 10000) || (country.currency === 'STN' && realQuantity >= 300) ?
                    <Link style={{ textDecoration: 'none' }} to={`${(country.name && quantity) ? '/buy-secondPage' : '/'}`}>
                        <Button className={classes.btn} variant="contained" color="secondary">
                            {t("buyFrontPage.button")}
                        </Button>
                    </Link> :
                    <Button className={classes.btn} variant="contained" disabled>
                        {t("buyFrontPage.button")}
                    </Button>
            }
        </Paper>
    );
};

export default BuyFrontPage;