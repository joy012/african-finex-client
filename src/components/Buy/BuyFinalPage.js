import { Box, Button, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { UserContext } from '../../App';

const useStyles = makeStyles((theme) => ({
    textLeft: {
        margin: '0.6rem 0'
    },
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    text: {
        color: 'gray',
        wordBreak: 'break-all',
        marginTop: '0rem'
    },
    break: {
        margin: '-0.15rem auto',
        wordBreak: 'break-all'
    },
    btn: {
        width: '100%',
        display: 'block',
        margin: '.8rem auto',
        textAlign: 'center',
        padding: "0.6rem 2em"
    }
}));

const BuyFinalPage = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const history= useHistory();
    const [country, , quantity, , wallet, , , , , , , , order, , time, , , , realQuantity] = useContext(UserContext);
    const partnerIban = country.currency === 'AOA' ? 'AO06 0067 0000 0026 0933 1028 0' : 'ST68 0002 0000 0141 7143 1011 5';
    const partnerBank = country.currency === 'AOA' ? 'Banco de Investimento Rural - (BIR)' : 'Banco Internacional de São Tomé e Príncipe';

    const download = () => {
        history.push('/buyPdf');
    }

    return (
        <Paper elevation={4} className={classes.buyCard}>
            <Typography variant='h5' className={classes.spacing}> {t("buyFinalPage.title")} </Typography>

            <Typography variant='body1'> {t("buyFinalPage.subtitleP1")}{quantity} T{country.currency}{t("buyFinalPage.subtitleP2")}</Typography>
            <Typography variant='body2' className={classes.text}>{wallet}</Typography>


            <Box>
                <Typography variant='h6' className={classes.textLeft}> {t("buyFinalPage.recipient")}  </Typography>


                <Grid container alignItems="center" spacing={3}>

                    <Grid item xs={4}>
                        <Typography className={classes.break}> {t("buyFinalPage.orderId")} </Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{order}</Typography>
                    </Grid>
                </Grid>

                <Grid container alignItems="center" spacing={3}>
                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("buyFinalPage.IBAN")}:</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{partnerIban}</Typography>
                    </Grid>
                </Grid>

                <Grid container alignItems="center" spacing={3}>

                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("buyFinalPage.Bank")}:</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{partnerBank}</Typography>
                    </Grid>
                </Grid>
                <Grid container alignItems="center" spacing={3}>

                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("buyFinalPage.Coin")}:</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{realQuantity} {country.currency}</Typography>
                    </Grid>
                </Grid>
                <Grid container alignItems="center" spacing={3}>

                    <Grid item xs={4}>
                        <Typography className={classes.break}>{t("buyFinalPage.Time")}:</Typography>
                    </Grid>
                    <Grid item xs={8}>
                        <Typography variant='subtitle2' className={classes.break}>{time}</Typography>
                    </Grid>
                </Grid>
            </Box>

            <Typography variant='body2' color='secondary' className={classes.spacing}> {t("buyFinalPage.desc")} </Typography>

            <Button color='secondary' variant='contained' className={classes.btn} onClick={download}> {t("buyFinalPage.button")}  </Button>
        </Paper>
    );
};

export default BuyFinalPage;