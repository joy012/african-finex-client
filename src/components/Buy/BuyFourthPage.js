import { Button, CircularProgress, FormControl, Grid, InputLabel, makeStyles, Paper, Select, TextField, Typography } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import React, { createRef, useContext, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import ReCAPTCHA from "react-google-recaptcha";
import { UserContext } from '../../App';
import { useTranslation } from 'react-i18next';
var IBAN = require('iban');


const useStyles = makeStyles((theme) => ({
    spacing: {
        margin: '0.8rem 0',
        textAlign: 'center'
    },
    select: {
        width: '100%'
    },
    input: {
        width: '100%',
    },
    buyCard: {
        width: '90%',
        borderRadius: '16px',
        display: 'block',
        margin: 'auto',
        marginBottom: '3rem',
        padding: '0.7rem 1rem'
    },
    formControl: {
        width: "100%",
        display: 'block',
        margin: '1.5rem auto',
    },
    text: {
        color: 'gray',
        wordBreak: 'break-all'
    },
    btn: {
        width: '100%',
        display: 'block',
        margin: '1rem auto 0.8rem auto',
        textAlign: 'center',
        padding: "0.6rem 2em"
    },
    captcha: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '0.5rem auto'
    },
}));

const BuyFourthPage = () => {
    const { t } = useTranslation();
    const classes = useStyles();
    const history = useHistory();
    const [country, , quantity, , wallet, , iban, setIban, , , bank, setBank, order, setOrder, time, setTime] = useContext(UserContext);
    const [ibanValid, setIbanValid] = useState(true);
    const recaptchaRef = createRef();
    const [captcha, setCaptcha] = useState('');
    const [dbInitiate, SetDbInitiate] = useState(true);
    const IbanPrefix = country.currency === 'AOA' ? 'AO06' : 'ST68';
    const postData = {
        country: country,
        currency: country.currency,
        coinQuantity: quantity,
        wallet: wallet,
        IBAN: iban,
        bank: bank,
        orderId: order,
        timeStamp: time
    };

    const handleAddBuy = e => {
        if (captcha) {
            SetDbInitiate(false);
            fetch('https://african-finex-server.herokuapp.com/buyCoin', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(postData)
            })
                .then(res => res.json())
                .then(result => {
                    if (result) {
                        SetDbInitiate(true);
                        history.push('/buy-finalPage');
                    }
                    else {
                        alert("For some Reason We can't connect to the server. Try Again Please")
                        history.push('/')
                    }
                })
        }
        else {
            alert('verify yourself')
        }

    }

    const handleBank = e => {
        setBank(e.target.value)
    }
    const handleIban = e => {
        const value = e.target.value;
        if (country.currency && value !== '') {
            const ibanValue = value.replace(/\s/g, "");
            if (country.currency[0] === ibanValue[0]) {
                if (ibanValue.length === 25 && (IBAN.isValid(ibanValue) || ibanValue === 'ST68000200000141714310115')) {
                    setIban(value);
                    setIbanValid(true);
                    setTime(new Date().toLocaleString());
                    setOrder(
                        country.currency === 'AOA' ? `AO${Math.floor(100000 + Math.random() * 900000)}` :
                            `ST${Math.floor(100000 + Math.random() * 900000)}`
                    )
                }
                else {
                    setIbanValid(false);
                    setIban('');
                }
            }
            else {
                setIbanValid(false);
                setIban('');
            }
        }

    }

    const goBack = () => {
        history.goBack(1);
    }

    const handleCaptcha = () => {
        const recaptchaValue = recaptchaRef.current.getValue();
        setCaptcha(recaptchaValue);
    }
 
    return (
        <Paper elevation={4} className={classes.buyCard}>
            <Grid container alignItems="center">
                <Grid item xs={1}>
                    <Link>
                        <ArrowBackIosIcon onClick={goBack} />
                    </Link>
                </Grid>
                <Grid item xs={10}>
                    <Typography variant='h6' className={classes.spacing}> {t("buyFourthPage.title")} </Typography>
                </Grid>
            </Grid>

            <Typography variant='body1'> {t("buyFourthPage.subtitle")}  </Typography>
            <Typography variant='body2' className={classes.text}>{wallet}</Typography>

            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="filled-coin-native-simple"> {t("buyFourthPage.inputLabel")}  </InputLabel>
                {
                    country.currency === 'AOA' &&
                    <Select
                        className={classes.select}
                        native
                        value={bank}
                        onChange={handleBank}
                        label={t("buyFourthPage.inputLabel")}
                        defaultValue='Select You Bank'
                    >
                        <option aria-label="None" value="" />
                        <option>Banco de Poupança e Crédito - (BPC)</option>
                        <option>Banco Comercial do Huambo - (BCH)</option>
                        <option>Banco BAI Microfinanças - (BMF)</option>
                        <option>Banco de Fomento Angola - (BFA)</option>
                        <option>Banco Comércio e Indústria - (BCI)</option>
                        <option>Banco Keve - (BKEVE)</option>
                        <option>Banco Sol - (BSOL)</option>
                        <option>Banco Económico - (BE)</option>
                        <option>Banco Millenium Atlântico - (ATL)</option>
                        <option>Banco BIC - (BIC)</option>
                        <option>Banco de Negócios Internacional - (BNI)</option>
                        <option>Banco de Investimento Rural - (BIR)</option>
                        <option>Standard Bank Angola - (SBA)</option>
                        <option>Banco Caixa Angola - (BCGA)</option>
                        <option>Banco Angolano de Investimentos - (BAI)</option>
                        <option>Banco Prestígio - (BPG)</option>
                        <option>Banco da China Limitada - Sucursal em Luanda - (BOCLB)</option>
                        <option>Banco Valor - (BVB)</option>
                        <option>VTB África - (VTB)</option>
                        <option>Banco Comercial Angolano - (BCA)</option>
                        <option>Standard Chartered Bank Angola - (SCBA)</option>
                        <option>Banco de Crédito do Sul - (BCS)</option>
                        <option>Finibanco Angola - (FNB)</option>
                        <option>Banco Yetu - (Yetu)</option>
                    </Select>
                }
                {
                    country.currency === 'STN' &&
                    <Select
                        className={classes.select}
                        native
                        value={bank}
                        onChange={handleBank}
                        label={t("buyFourthPage.inputLabel")}
                        defaultValue='Select You Bank'
                    >
                        <option aria-label="None" value="" />
                        <option>Afriland First Bank</option>
                        <option>Banco Internacional de São Tomé e Príncipe</option>
                        <option>BGFI Babk STP</option>
                        <option>Banco Internacional de STP</option>
                        <option>Ecobank</option>
                    </Select>
                }
            </FormControl>

            {
                bank &&
                <FormControl variant="outlined" className={classes.formControl}>
                    <TextField onChange={handleIban} className={classes.input} id="outlined-search" label={t("buyFourthPage.inputLabel2")} variant="outlined" defaultValue={IbanPrefix} autoComplete="off" />
                </FormControl>
            }
            {
                !ibanValid &&
                <Typography variant='subtitle1' color='secondary'>{t("buyFourthPage.err")} </Typography>
            }
            {
                bank && iban &&
                <ReCAPTCHA
                    ref={recaptchaRef}
                    className={classes.captcha}
                    sitekey="6LcSvVIaAAAAAI5CwgAlMiWNqVr4K8GU-LqHe-Xx"
                    onChange={handleCaptcha}
                />

            }

            {
                bank && iban && captcha ?
                    <Link style={{ textDecoration: 'none' }}>
                        {
                            dbInitiate &&
                            <Button onClick={handleAddBuy} className={classes.btn} variant="contained" color="secondary">
                                {t("buyFourthPage.button")}
                            </Button>
                        }
                        {
                            !dbInitiate &&
                            <Button className={classes.btn} variant="contained" color="secondary">
                                <CircularProgress />
                            </Button>
                        }

                    </Link> :
                    <Button className={classes.btn} variant="contained" disabled>
                        {t("buyFourthPage.button")}
                    </Button>
            }

        </Paper>
    );
};

export default BuyFourthPage;