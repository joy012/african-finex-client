import React, { useContext } from 'react';
import './Navbar.css';
import i18n from '../../i18n';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import logo from '../../image/AfricaFinexLogo.png';
import { useState } from 'react';
import { useRef } from 'react';
import { useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Box, Button, Container, FormControl, InputLabel, Menu, MenuItem, Select, useMediaQuery } from '@material-ui/core';
import enIcon from "../../image/icons/en.png";
import ptIcon from "../../image/icons/pt.png";
import { useTranslation } from 'react-i18next';
import { UserContext } from '../../App';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: '4rem'
    },
    tool: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    headerOptions: {
        display: 'flex',
        justifyContent: 'space-evenly'
    },
    navlink: {
        fontSize: '1rem',
        padding: '0.5rem 2.5rem',
        color: '#fff'
    },
    logo: {
        width: '15%',
        [theme.breakpoints.down("md")]: {
            width: '15%'
        },
        [theme.breakpoints.down("sm")]: {
            width: '30%'
        },
        pointerEvents: 'none'
    },
    appBarSolid: {
        backgroundColor: 'rgba(25,118,210)',
    },
    appBarTransparent: {
        backgroundColor: 'rgba(25,118,210, 0.4)',
    },
    formControl: {
        textTransform: "uppercase",
    }
}));

const Navbar = () => {
    const { t } = useTranslation();
    const [, , , , , , , , , , , , , , , , , setLanguage] = useContext(UserContext);
    const history = useHistory();
    const classes = useStyles();
    const [navBG, setNavBg] = useState('appBarSolid');
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("xs"));
    const navRef = useRef();
    navRef.current = navBG;

    const handleMenu = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleMenuClick = pageURL => {
        history.push(pageURL);
        setAnchorEl(null);
    };

    const menuItems = [
        {
            menuTitle: "Buy",
            pageURL: "/"
        },
        {
            menuTitle: "Sell",
            pageURL: "/sell-frontPage"
        }
    ];

    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 200) {
                setNavBg('appBarTransparent');
            }
            else {
                setNavBg('appBarSolid');
            }
        }
        document.addEventListener('scroll', handleScroll);

        return () => {
            document.removeEventListener('scroll', handleScroll);
        }

    }, [])



    const detectingUserLang = navigator.language || navigator.userLanguage;
    const userLangFirst = detectingUserLang.split("-")[0];
    const [lang, setLang] = useState(userLangFirst);
    const changeLanguage = (lng) => {
        i18n.changeLanguage(lng);
        setLanguage(lng);
    }

    const handleSelect = (e) => {
        const currentValue = e.target.value;
        setLang(currentValue);
    }

    return (
        <div id="header" className={`header ${classes.root}`}>
            <AppBar position="static" className={classes[navRef.current]}>
                <Container>
                    <Toolbar className={classes.tool}>
                        <img src={logo} alt="logo" className={classes.logo} />

                        {isMobile ? (
                            <>
                                <IconButton
                                    edge="start"
                                    className={classes.menuButton}
                                    color="inherit"
                                    aria-label="menu"
                                    onClick={handleMenu}
                                >
                                    <MenuIcon />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: "top",
                                        horizontal: "right"
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: "top",
                                        horizontal: "right"
                                    }}
                                    open={open}
                                    onClose={() => setAnchorEl(null)}
                                >
                                    {menuItems.map(menuItem => {
                                        const { menuTitle, pageURL } = menuItem;
                                        return (
                                            <MenuItem onClick={() => handleMenuClick(pageURL)}>
                                                {menuTitle}
                                            </MenuItem>
                                        );
                                    })}
                                    <FormControl variant="filled" className={classes.formControl} >
                                        <InputLabel id="demo-simple-select-filled-label" className={classes.formControl}>Lang</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-filled-label"
                                            id="demo-simple-select-filled"
                                            displayEmpty
                                            value={lang}
                                            onChange={e => handleSelect(e)}
                                            label="Lang"
                                            inputProps={{
                                                name: 'Lang',
                                                id: 'demo-simple-select-filled',
                                            }}

                                            className={classes.formControl}
                                        >
                                            <MenuItem value={"en"} onClick={() => changeLanguage('en')}> <img src={enIcon} alt="" className="icon" /> English </MenuItem>

                                            <MenuItem value={"pt"} onClick={() => changeLanguage('pt')}> <img src={ptIcon} alt="" className="icon" /> Português </MenuItem>

                                        </Select>
                                    </FormControl>
                                </Menu>
                            </>
                        ) : (
                                <Box className={classes.headerOptions}>
                                    <Button className={classes.navlink}>
                                        <Link style={{ textDecoration: 'none', color: 'white' }} to='/'>
                                            {t("buy")}
                                        </Link>
                                    </Button>

                                    <Button className={classes.navlink}>
                                        <Link style={{ textDecoration: 'none', color: 'white' }} to='/sell-frontPage'>
                                            {t("sell")}
                                        </Link>
                                    </Button>

                                    <FormControl variant="filled" className={classes.formControl} >
                                        <InputLabel id="demo-simple-select-filled-label" className={classes.formControl}>Lang</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-filled-label"
                                            id="demo-simple-select-filled"
                                            displayEmpty
                                            value={lang}
                                            onChange={e => handleSelect(e)}
                                            label="Lang"
                                            inputProps={{
                                                name: 'Lang',
                                                id: 'demo-simple-select-filled',
                                            }}

                                            className={classes.formControl}
                                        >
                                            <MenuItem value={"en"} onClick={() => changeLanguage('en')}> <img src={enIcon} alt="" className="icon" /> English </MenuItem>

                                            <MenuItem value={"pt"} onClick={() => changeLanguage('pt')}> <img src={ptIcon} alt="" className="icon" /> Português </MenuItem>

                                        </Select>
                                    </FormControl>
                                </Box>
                            )}
                    </Toolbar>
                </Container>
            </AppBar>
        </div>
    );
};

export default Navbar;